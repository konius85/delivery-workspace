package database

import (
	"github.com/go-pg/pg/v9"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/database/postgres"
)

type Connection interface {
	Connect()
}

type T struct{}

func (c T) PgConnect() *pg.DB {
	return postgres.PgConnect()
}
