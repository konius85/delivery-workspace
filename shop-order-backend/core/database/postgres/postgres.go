package postgres

import (
	"strconv"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/config"

	"github.com/go-pg/pg/v9"
)

func PgConnect() *pg.DB {
	addr := config.Config.Postgres.Host + ":" + strconv.Itoa(config.Config.Postgres.Port)
	return pg.Connect(&pg.Options{
		Addr:     addr,
		User:     config.Config.Postgres.User,
		Database: config.Config.Postgres.Database,
		Password: config.Config.Postgres.Password,
	})
}
