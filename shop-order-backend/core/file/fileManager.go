package file

import (
	"crypto/rand"
	"fmt"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/config"
)

func UploadFile(w http.ResponseWriter, r *http.Request) (*models.File, error) {
	var maxUploadSize = config.Config.MaxUploadFileSize
	var maxFile = int64(maxUploadSize << 20)

	r.Body = http.MaxBytesReader(w, r.Body, maxFile)
	if err := r.ParseMultipartForm(maxFile); err != nil {
		log.Println(err)
		return renderMessage("FILE_TOO_BIG")
	}

	file, handler, err := r.FormFile("file")

	if err != nil {
		log.Println(err)
		return renderMessage("INVALID_FILE")
	}
	defer file.Close()
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return renderMessage("CAN_NOT_READ_FILE")
	}

	detectedFileType := http.DetectContentType(fileBytes)

	switch detectedFileType {
	case "image/jpeg", "image/jpg":
	case "image/gif", "image/png":
	case "application/pdf":
		break
	default:
		return renderMessage("INVALID_FILE_TYPE")
	}

	filesPath, err := generateFilePath()
	if err != nil {
		return renderMessage("CAN_NOT_GENERATE_FILE_PATH")
	}

	fileName, err := randToken(12)
	if err != nil {
		return renderMessage("CANT_GENERATE_TOKEN")
	}
	fileEndings, err := mime.ExtensionsByType(detectedFileType)
	if err != nil {
		return renderMessage("CAN_NOT_GET_FILE_EXTENSION")
	}

	newPath := filepath.Join(filesPath, fileName+fileEndings[0])

	err = ioutil.WriteFile(newPath, fileBytes, 0644)
	if err != nil {
		return renderMessage("CAN_NOT_WRITE_FILE")
	}

	fileData := new(models.File)
	fileData.OriginalName = handler.Filename
	fileData.Size = handler.Size
	fileData.MimeType = fmt.Sprint(handler.Header.Get("Content-Type"))
	fileData.Extension = detectedFileType
	fileData.Name = newPath
	fileData.Directory = filesPath
	return fileData, nil
}

// Method generate file path
// @return ./file/2020-11-11/, error
func generateFilePath() (string, error) {
	filePath := config.Config.FilePath
	fullPath := filePath + "/" + getTimeAsString() + "/"
	err := os.MkdirAll(fullPath, 0777)
	return fullPath, err
}

func getTimeAsString() string {
	t := time.Now()
	year := t.Year()
	month := int(t.Month())
	day := t.Day()
	return fmt.Sprintf("%d-%d-%d", year, month, day)
}

func randToken(len int) (string, error) {
	b := make([]byte, len)
	_, err := rand.Read(b)
	return fmt.Sprintf("%x", b), err
}

func renderMessage(message string) (*models.File, error) {
	return &models.File{}, fmt.Errorf(message)
}
