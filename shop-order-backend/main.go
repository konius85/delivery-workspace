package main

import (
	"fmt"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/routes"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/core/config"
)

func init() {
	config.LoadConfig()
}

func handleRequest() {
	router := mux.NewRouter().StrictSlash(true)
	log.Println("PORTSSSSSSSSSSSS", config.Config.Postgres)
	fmt.Println("sasssssssssssssssssssssssssssssssssssssssss", config.Config.Postgres)
	routes.RegisterRoutes(router)

	appPort := fmt.Sprint(config.Config.AppPort)
	log.Println("Server is running and listening on port " + appPort)
	log.Fatal(http.ListenAndServe(":"+appPort, handlers.CORS()(router)))
}

func main() {
	handleRequest()
}
