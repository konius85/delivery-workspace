module gitlab.com/konius85/delivery-workspace/shop-order-backend

go 1.14

require (
	github.com/go-pg/migrations v6.7.3+incompatible
	github.com/go-pg/pg v8.0.6+incompatible // indirect
	github.com/go-pg/pg/v9 v9.1.6
	github.com/go-pg/urlstruct v0.4.0 // indirect
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/configor v1.2.0
	github.com/segmentio/encoding v0.1.11 // indirect
	github.com/vmihailenco/bufpool v0.1.11 // indirect
	github.com/vmihailenco/msgpack/v4 v4.3.11 // indirect
	golang.org/x/crypto v0.0.0-20200427165652-729f1e841bcc // indirect
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0 // indirect
	google.golang.org/appengine v1.6.6 // indirect
)

