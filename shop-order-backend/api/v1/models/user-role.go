package models

type UserRole struct {
	Id    int64
	Name  string `pg:"type:varchar(255)"`
	Users []*User
}
