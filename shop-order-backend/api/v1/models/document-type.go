package models

import "fmt"

type DocumentType struct {
	Id    int8
	Type  string
	IsDel bool `pg:"default:false"`
}

func (d DocumentType) String() string {
	return fmt.Sprintf("DocumentType<%d %s %t>", d.Id, d.Type, d.IsDel)
}
