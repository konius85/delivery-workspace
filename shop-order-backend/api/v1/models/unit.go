package models

type Unit struct {
	Id        int8   `pg:",pk"`
	ShortName string `pg:"type:varchar(25)"`
	Name      string
}
