package models

import (
	"fmt"
	"time"
)

type User struct {
	Id         int64
	FName      string
	LName      string
	DateOfBird time.Time
	Email      string
	Password   string
	IsDel      bool `pg:"default:false"`
	UserRoleId int
	UserRole   *UserRole
	Files      []*File
}

func (u User) String() string {
	return fmt.Sprintf("User<%d %s %v>", u.Id, u.FName, u.LName, u.DateOfBird, u.Email, u.Password, u.IsDel)
}

func AdminUser() *User {
	adminUser := &User{
		FName:    "admin",
		LName:    "admin",
		Email:    "admin1@admin",
		Password: "admin",
		IsDel:    true,
	}
	return adminUser
}
