package document

import (
	"encoding/json"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/services/dbService"

	"net/http"
)

func CreateDocumentType(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	w.Header().Set("Content-Type", "text/json")
	var Data models.DocumentType

	err := json.NewDecoder(r.Body).Decode(&Data)
	err = con.Insert(&Data)
	err = json.NewEncoder(w).Encode(Data)
	if err != nil {
		panic(err)
		return
	}
}

func UpdateDocumentType(w http.ResponseWriter, r *http.Request) {

}

func DeleteDocumentType(w http.ResponseWriter, r *http.Request) {

}
