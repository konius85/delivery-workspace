package routes

import (
	"github.com/gorilla/mux"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/routes/document"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/routes/product"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/routes/unit"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/services/dbService"
)

func RegisterRoutes(router *mux.Router) {
	apiRouters(router)
}

func apiRouters(router *mux.Router) {
	createDatabases()
	api := router.PathPrefix("/api/v1/").Subrouter()

	//Document routers
	api.HandleFunc("/documents", document.GetAll).Methods("GET")
	api.HandleFunc("/documents", document.Create).Methods("POST")
	api.HandleFunc("/documents/{documentId}", document.Update).Methods("PUT")
	api.HandleFunc("/documents/{documentId}", document.Delete).Methods("DELETE")

	//Document Type routers
	api.HandleFunc("/documentType", document.CreateDocumentType).Methods("POST")
	api.HandleFunc("/documentType/{docType}", document.UpdateDocumentType).Methods("PUT")
	api.HandleFunc("/documentType/{docType}", document.DeleteDocumentType).Methods("DELETE")

	//Unit routers
	api.HandleFunc("/units", unit.GetAll).Methods("GET")

	//Products
	api.HandleFunc("/products", product.GetAll).Methods("GET")
	api.HandleFunc("/products", product.Create).Methods("POST")

}

func createDatabases() {
	var modelsI = []interface{}{
		(*models.Document)(nil),
		(*models.DocumentType)(nil),
		(*models.UserRole)(nil),
		(*models.User)(nil),
		(*models.File)(nil),
		(*models.Unit)(nil),
		(*models.Product)(nil),
	}

	err := dbService.CreateSchema(modelsI)

	if err != nil {
		panic(err)
	}
}
