package unit

import (
	"encoding/json"
	"net/http"

	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/models"
	"gitlab.com/konius85/delivery-workspace/shop-order-backend/api/v1/services/dbService"
)

func GetAll(w http.ResponseWriter, r *http.Request) {
	con := dbService.PgConnection()
	defer con.Close()

	var units = make([]models.Unit, 0)
	err := con.Model(&units).Select()
	if err != nil {
		panic(err)
	}

	type respond struct {
		Units []models.Unit `json:"units"`
	}

	res := respond{
		Units: units,
	}
	err = json.NewEncoder(w).Encode(res)
	if err != nil {
		panic(err)
	}
}
